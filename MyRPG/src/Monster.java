import java.util.Random;

public class Monster extends Character {

	Random random = new Random();

	public Monster(String _name) {
		super(_name);
	}

	public Monster(String _name, int maxLife) {
		super(_name,maxLife);
	}

	public Monster(String _name, int maxLife, int life) {
		super(_name,maxLife,life);
	}

	public Monster(String _name, int maxLife, int life, int maxMagic) {
		super(_name,maxLife,life,maxMagic);
	}

	public Monster(String _name, int maxLife, int life, int maxMagic, int magic) {
		super(_name,maxLife,life,maxMagic,magic);
	}

	public Monster(String _name, int maxLife, int life, int maxMagic, int magic, int hitPoint) {
		super(_name,maxLife,life,maxMagic,magic,hitPoint);
	}

	public Monster(String _name, int maxLife, int life, int maxMagic, int magic, int hitPoint, int speedPoint) {
		super(_name,maxLife,life,maxMagic,magic,hitPoint,speedPoint);
	}

	public Monster(String _name, int maxLife, int life, int maxMagic, int magic, int hitPoint, int speedPoint, int specialAttackFlag) {
		super(_name,maxLife,life,maxMagic,magic,hitPoint,speedPoint,specialAttackFlag);
	}
	public Monster(String _name, int maxLife, int life, int maxMagic, int magic,
			int hitPoint, int speedPoint, int specialAttackFlag, int damage) {
		super(_name,maxLife,life,maxMagic,magic,hitPoint,speedPoint,specialAttackFlag,damage);
	}

	@Override
	public void sayHello() {
		System.out.println("俺は" + this.getName() + "だ！");
	}

	@Override
	public int attack(Character c) { // 攻撃対象
		Random random = new Random();
		c.setDamage(this.getHitPoint() + random.nextInt(10));

		specialAttack(c); // 現在HPに応じた特殊行動

		while(true) {
			switch(random.nextInt(6)) {
			case 0:
				if(this.getName() == "Lucifer" || this.getName() == "Alice") {
					continue;
				}
				System.out.println(this.getName()+"は、様子を見ている。");
				break;
			case 1:
				System.out.println();
				System.out.println(this.getName()+"の攻撃！");
				avoidance(c);
				c.setLife(c.getLife()-c.getDamage());
				System.out.println(this.getName()+"は、"+c.getName()+"に"+c.getDamage()+"のダメージを与えた！");
				break;
			case 2:
				System.out.println();
				System.out.println(this.getName()+"の攻撃！");
				avoidance(c);
				c.setLife(c.getLife()-c.getDamage());
				System.out.println(this.getName()+"は、"+c.getName()+"に"+c.getDamage()+"のダメージを与えた！");
				break;
			case 3: // モンスター特有の攻撃

				if(this.getName() == "バイオレンスおやじ") {
					this.setLife(this.getLife()-10);
					System.out.println();
					System.out.println(this.getName()+"の特殊攻撃！");
					System.out.println(this.getName()+":「飛び膝蹴り！」");
					avoidance(c);
					c.setLife(c.getLife()-c.getDamage());
					System.out.println(this.getName()+"は、"+c.getName()+"に"+c.getDamage()+"のダメージを与えた！");
					System.out.println(this.getName()+"は、反動で10のダメージを受けた！");
					break;
				}

				if(this.getName() == "炸裂する小悪魔") {
					c.setLife(c.getLife()-this.getLife());
					System.out.println();
					System.out.println(this.getName()+"の特殊攻撃！");
					System.out.println(this.getName()+"は身体を膨張させ、爆発した！");
					System.out.println(this.getName()+"は自身を犠牲にして、"+c.getName()+"に"+this.getLife()+"のダメージを与えた！");
					this.setLife(this.getLife()-this.getMaxLife());
					break;
				}

				if(this.getName() == "渇きの老婆") {
					if(this.getMagic() < 10) {
						System.out.println();
						System.out.println(this.getName()+": み、水を飲みすぎて、動け・・・ない・・・。");
						break;
					}
					this.setMagic(this.getMagic()-10);
					System.out.println();
					System.out.println(this.getName()+": 水じゃ・・・！泉の水じゃ！！");
					if(this.getMaxLife() < this.getLife() + 50) {
						this.setLife(this.getMaxLife());
						System.out.println(this.getName()+"は、Lifeを全回復した！");
					} else {
						this.setLife(this.getLife()+50);
						System.out.println(this.getName()+"は、50ポイントのLifeを回復した！");
					}
					break;
				}

				if(this.getName() == "ケルベロス") {
					System.out.println();
					System.out.println(this.getName()+"の特殊攻撃！");
					System.out.println(this.getName()+":「冥府の業火に焼かれるがよい！ ピュリプレゲトーン！！！」");
					for(int i=0; i<3; i++) {
						c.setDamage(this.getHitPoint() + random.nextInt(10));
						avoidance(c);
						c.setLife(c.getLife()-c.getDamage());
						System.out.println(this.getName()+"は、"+c.getName()+"に"+c.getDamage()+"のダメージを与えた！");
					}
					break;
				}

				if(this.getName() == "デーモンコア") {
					System.out.println();
					System.out.println(this.getName()+"の特殊攻撃！");
					System.out.println(this.getName()+"から放たれる瘴気が"+c.getName()+"の周りにまとわりつく！");
					for(int i=0; i<5; i++) {
						c.setDamage((this.getHitPoint() + random.nextInt(10))/2);
						avoidance(c);
						c.setLife(c.getLife()-c.getDamage());
						System.out.println(this.getName()+"は、"+c.getName()+"に"+c.getDamage()+"のダメージを与えた！");
					}
					break;
				}

				if(this.getName() == "Lucifer") {
					if(this.getMagic() < 10) {
						continue;
					}
					this.setMagic(this.getMagic()-10);
					System.out.println();
					System.out.println(this.getName()+": 退屈しのぎにもならぬな・・・。");
					if(this.getMaxLife() < this.getLife() + 80) {
						this.setLife(this.getMaxLife());
						System.out.println(this.getName()+"は、Lifeを全回復した！");
					} else {
						this.setLife(this.getLife()+80);
						System.out.println(this.getName()+"は、80ポイントのLifeを回復した！");
					}
					break;
				}

				if(this.getName() == "Alice") {
					if(this.getMagic() < 10) {
						continue;
					}
					this.setMagic(this.getMagic()-10);
					System.out.println();
					System.out.println(this.getName()+": ちょっときゅうけーい！");
					if(this.getMaxLife() < this.getLife() + 80) {
						this.setLife(this.getMaxLife());
						System.out.println(this.getName()+"は、Lifeを全回復した！");
					} else {
						this.setLife(this.getLife()+80);
						System.out.println(this.getName()+"は、80ポイントのLifeを回復した！");
					}
					break;
				}
			case 4: // ボス特有の行動パターン
				if(this.getName() == "Lucifer") {
					System.out.println();
					System.out.println(this.getName()+": お前のような者を何度も葬ってきた。");
					System.out.println(this.getName()+": 皆、最期はこの世の全てに絶望をし、地獄へ落ちていった。");
					System.out.println(this.getName()+": お前も例外ではない。希望を捨てると楽になれるぞ？");
					break;
				} else if (this.getName() == "Alice") { 
					
				} else { // 他のモンスターは行動パターンの抽選し直し。
					continue;
				}
				
				
			case 5: // ボス特有の行動パターン
				if(this.getName() == "Lucifer") {
					if(this.getMagic() < 10) {
						continue;
					}
					this.setMagic(this.getMagic()-10);
					System.out.println();
					System.out.println(this.getName()+": 光を打ち消す闇よ！飛散しろ！");
					for(int i=0; i<10; i++) {
						c.setDamage(random.nextInt(10));
						avoidance(c);
						c.setLife(c.getLife()-c.getDamage());
						System.out.println(this.getName()+"は、"+c.getName()+"に"+c.getDamage()+"のダメージを与えた！");
					}
					break;
				} else { // 他のモンスターは行動パターンの抽選し直し。
					continue;
				}
			}
			break;
		}
		return c.getLife();
	}

	public void specialAttack(Character c){
		int damage = this.getHitPoint() + random.nextInt(10);
		if(this.getName() == "Lucifer") {
			if(this.getLife() <= 1000 && this.getSpecialAttackFlag() == 0) {
				System.out.println();
				System.out.println(this.getName()+": 少しは骨のある奴のようだな・・・？");
				System.out.println(this.getName()+": だが、我を満足させるには程遠いぞ！");
				this.setSpecialAttackFlag(1);
			} else if(this.getLife() <= 500 && this.getSpecialAttackFlag() == 1) {
				System.out.println();
				System.out.println(this.getName()+": そう焦るな、人の子よ。");
				System.out.println(this.getName()+": 人生というものは長いのであろう？");
				System.out.println(this.getName()+": 我にとっては、瞬き一つで終わってしまうのだがな。");
				this.setLife(this.getLife()+300);
				System.out.println(this.getName()+"は、300ポイントのLifeを回復した！");
				this.setSpecialAttackFlag(2);
			} else if(this.getLife() <= 100 && this.getSpecialAttackFlag() == 2) {
				System.out.println();
				System.out.println(this.getName()+": くっ・・・！");
				System.out.println(this.getName()+": たかが人の子だと見誤っていたようだ。");
				System.out.println(this.getName()+": お前は他の者よりは強い。");
				System.out.println(this.getName()+": しかし、これで終わりだ！");
				System.out.println(this.getName()+": この世界に天よりの雷を与えん！");
				System.out.println(this.getName()+": メキドアーク！！！");
				System.out.println();
				this.setSpecialAttackFlag(3);
				for(int i=0; i<5; i++) {
					damage =(random.nextInt(10)+10);
					avoidance(c);
					c.setLife(c.getLife()-c.getDamage());
					System.out.println(this.getName()+"は、"+c.getName()+"に"+(damage)+"のダメージを与えた！");
				}
			}
		}
		System.out.println(this.getSpecialAttackFlag());
	}

	public int avoidance(Character c){
		Random random = new Random();
		// 10%か20％の確率で攻撃が回避される。
		switch (random.nextInt(10)) {
		case 0:
			c.setDamage(0);
			System.out.println("しかし、"+c.getName()+"は、攻撃を回避した！");
			break;
		case 1:
			if(this.getSpeedPoint() < c.getSpeedPoint()) { // 相手の素早さの方が高い場合
				c.setDamage(0);
				System.out.println("しかし、"+this.getName()+"は、攻撃を回避した！");
			}
			break;
		default:
			break;
		}
		return c.getDamage();
	}
}
