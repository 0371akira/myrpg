public class Event {

	// 列挙型
	enum EventType {
		Talk,	 // 会話のイベント
		Damage,  // ダメージのイベント
		Care,    // 回復のイベント
		Battle,   // 戦いのイベント
		BossBattle,  // ラスボスとの戦いのイベント
		AliceBattle   // ラスボスとの戦いのイベント
	}

	private EventType 	type;			// イベントタイプ
	private String    	name;			// イベント名
	private int		damagePoint;    // ダメージポイント
	private int    	careLife;		// Life回復ポイント
	private int    	careMagic;		// Magic回復ポイント
	private Monster 	monster;		// モンスター


	// コンストラクタ
	public Event( EventType _type, String _name){
		this.name = _name;
		this.type = _type;
	}

	public EventType getType() {
		return type;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public int getDamagePoint() {
		return damagePoint;
	}
	public void setDamagePoint(int damagePoint) {
		this.damagePoint = damagePoint;
	}

	public int getCareLife() {
		return careLife;
	}

	public void setCareLife(int careLife) {
		this.careLife = careLife;
	}

	public int getCareMagic() {
		return careMagic;
	}

	public void setCareMagic(int careMagic) {
		this.careMagic = careMagic;
	}

	public Monster getMonster() {
		return monster;
	}
	public void setMonster(Monster monster) {
		this.monster = monster;
	}

}
