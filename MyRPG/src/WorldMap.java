
public class WorldMap {

	private int 		width;		// 世界の大きさ (幅)
	private int 		height;		// 世界の大きさ (高さ)
	private Event[][]  map;			// イベント格納用の配列

	// コンストラクタ
	public WorldMap(){
		this.width = 5;
		this.height = 5;

		map = new Event[height][width];
	}

	public WorldMap( int _width, int _height ){
		if ( _width < 0 || _height < 0 ){
			this.width = 5;
			this.height = 5;
		}
		else {
			this.width = _width;
			this.height = _height;
		}

		map = new Event[height][width];
	}

	public int getWidth() {
		return width;
	}
	public int getHeight() {
		return height;
	}

	public Event getEvent( int x, int y ){
		if ( x < 0 || x >= this.width ) return null;
		if ( y < 0 || y >= this.height ) return null;

		return map[y][x];
	}
	public void setEvent(Event e, int x, int y ){
		if ( x < 0 || x >= this.width ) return;
		if ( y < 0 || y >= this.height ) return;

		map[y][x] = e;
	}
}
