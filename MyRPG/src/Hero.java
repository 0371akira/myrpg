import java.util.Random;
import java.util.Scanner;

public class Hero extends Character {
	Scanner scanner = new Scanner(System.in);

	public Hero(String _name) {
		super(_name);
	}

	public Hero(String _name, int maxLife) {
		super(_name,maxLife);
	}

	public Hero(String _name, int maxLife, int life) {
		super(_name,maxLife,life);
	}

	public Hero(String _name, int maxLife, int life, int maxMagic) {
		super(_name,maxLife,life,maxMagic);
	}

	public Hero(String _name, int maxLife, int life, int maxMagic, int magic) {
		super(_name,maxLife,life,maxMagic,magic);
	}

	public Hero(String _name, int maxLife, int life, int maxMagic, int magic, int hitPoint) {
		super(_name,maxLife,life,maxMagic,magic,hitPoint);
	}

	public Hero(String _name, int maxLife, int life, int maxMagic, int magic, int hitPoint, int speedPoint) {
		super(_name,maxLife,life,maxMagic,magic,hitPoint,speedPoint);
	}

	public Hero(String _name, int maxLife, int life, int maxMagic, int magic, int hitPoint, int speedPoint, int damage) {
		super(_name,maxLife,life,maxMagic,magic,hitPoint,speedPoint,damage);
	}

	public Hero(String _name, int maxLife, int life, int maxMagic, int magic, int hitPoint, int speedPoint, int damage, int killMonster) {
		super(_name,maxLife,life,maxMagic,magic,hitPoint,speedPoint,damage,killMonster);
	}

	@Override
	public void sayHello() {
		System.out.println("わたしは" + this.getName() + "だ。");
	}

	@Override
	public int attack(Character c) { // 攻撃対象
		Random random = new Random();
		c.setDamage((this.getHitPoint() + random.nextInt(10)));

		System.out.println();
		System.out.println("プレイヤーのターンです。どうしますか？");
		System.out.println("1: 物理攻撃        3: 魔法攻撃");
		System.out.println("2: 回復魔法        4: 逃走");
		System.out.print("選択肢: ");

		while(true) {
			switch(scanner.nextLine()) {
			case "1":
				System.out.println();
				System.out.println(this.getName()+"の攻撃！");
				avoidance(c);
				c.setLife(c.getLife()-c.getDamage());
				System.out.println(this.getName()+"は、"+c.getName()+"に"+c.getDamage()+"のダメージを与えた！");
				break;

			case "2":
				if(this.getMagic() < 20) {
					System.out.println("Magicが足りない！");
					System.out.println("選択肢: ");
					continue;
				}
				this.setMagic(this.getMagic()-20);
				System.out.println();
				System.out.println(this.getName()+"の回復魔法！");

				if(this.getMaxLife() < this.getLife() + 50) {
					this.setLife(this.getMaxLife());
					System.out.println(this.getName()+"は、Lifeを全回復した！ Magicを20消費した。");
				} else {
					this.setLife(this.getLife()+50);
					System.out.println(this.getName()+"は、50ポイントのLifeを回復した！ Magicを20消費した。");
				}
				break;

			case "3":
				if(this.getMagic() < 10) {
					System.out.println("Magicが足りない！");
					System.out.println("選択肢: ");
					continue;
				}
				this.setMagic(this.getMagic()-10);
				this.setHitPoint(50);
				System.out.println();
				System.out.println(this.getName()+"の魔法攻撃！");
				System.out.println(this.getName()+"「光あれ！！！」");
				avoidance(c);
				c.setLife(c.getLife()-c.getDamage());
				System.out.println(this.getName()+"は、"+c.getName()+"に"+c.getDamage()+"のダメージを与えた！Magicを10消費した。");
				break;

			case "4":
				c.setEscapeFlag(1);
				System.out.println();
				System.out.println(this.getName()+"は逃走した！");
					switch(random.nextInt(5)) {
					case 0:
						// 20%の確率で逃走失敗
						c.setEscapeFlag(0);
						System.out.println("しかし、"+this.getName()+"は転んでしまった！");
					default:
						break;
					}
				break;
			default :
				System.out.println("もう一度入力してください。");
				System.out.println("選択肢: ");
				continue;
			}
			break;
		}
		return c.getLife();
	}
	public int avoidance(Character c){
		Random random = new Random();
		// 10%か20％の確率で攻撃が回避される。
		switch (random.nextInt(10)) {
		case 0:
			c.setDamage(0);
			System.out.println("しかし、"+c.getName()+"は、攻撃を回避した！");
			break;
		case 1:
			if(this.getSpeedPoint() < c.getSpeedPoint()) { // 相手の素早さの方が高い場合
				c.setDamage(0);
				System.out.println("しかし、"+this.getName()+"は、攻撃を回避した！");
			}
			break;
		default:
			break;
		}
		return c.getDamage();
	}
}
