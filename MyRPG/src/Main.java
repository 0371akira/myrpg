// read me

/*
追加要素:
1. ゲームの終了コマンドの実装。
2. 会話イベントの実装。
3. ラスボスの実装。
4. 最大HP、最大MP、素早さ、魔法、モンスターの特殊攻撃の実装。
5. 戦闘の行動順をランダム化。（素早さによって行動順の偏りを出すようにした）
6. 戦闘中の逃げるコマンドの実装。 (20%の確率で逃走失敗)
7. オープニング、エンディングの実装。
8. ワールドマップとプレイヤーの現在地の可視化。
9. ラスボスの現在HPを不可視化。
	(一定値を下回ると見える。)
10. イベントがnullの時にも、ランダムで文章を表示。
11. ゲームオーバー処理の実装。
	(セリフは3パターン。ダメージゾーンでは開始時点に戻る。戦闘時は戦闘の始まりに戻る)
12. 攻撃のダメージポイントのランダム化。
13. ラスボスのHPが一定の値になったときの特殊行動を実装。
14. 戦闘中の回避行動の実装。
15. 裏ボスの実装。

バグ修正:
1. 最大HPがないため、無限に回復できるバグ（仕様？）の修正
2. 再度、同じ座標のモンスターと戦闘した時に、モンスターのHPが０のままになっているバグの修正
3. 主人公の名前を空白にできるバグの修正


やることリスト:
アリスの固有攻撃実装
各ステータスの調整

マップ:
{開始地点,会話		,null	,ダメージ小,Sariel  }
{会話   ,戦闘		,会話	,null	 ,戦闘	  }
{回復の泉,会話		,Alice	,会話	 ,戦闘	  }
{戦闘   ,ダメージ大,戦闘	,会話	 ,ダメージ大}
{Lilith,null	,回復の泉,null	 ,Lucifer }
 */

import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		int endFlag = 0;
		int clearFlag = 0;
		int gameOverFlag = 0;

		gameStart();
		Scanner scanner = new Scanner(System.in);
		WorldMap map = makeMap(scanner);
		String[][] watchingMap = new String[5][5];
		Hero h = makeHero(scanner);
		h.setXY(0, 0);

		while(h.getLife() > 0) {
			int x = h.getX();
			int y = h.getY();
			System.out.println("-----------------------------------------");
			System.out.println("「"+h.getName()+"」"+" life:["+h.getLife()+"] magic:["+h.getMagic()+"]");
			System.out.println("-----------------------------------------");
			System.out.println("どこに向かいますか？");
			System.out.print("北:8 東:6 南:2 西:4 マップを見る:1 ゲームの終了:0 > ");
			// 方向の入力
			while(true) {
				Scanner sc = new Scanner(System.in);
				int direction = sc.nextInt();
				switch(direction) {
				case 8:
					if(y > 0) { // 北
						y--;
						break;
					} else {
						continue;
					}
				case 6:
					if(x < map.getWidth()-1) {
						x++;
						break;
					} else {
						continue;
					}
				case 4:
					if(x > 0) {
						x--;
						break;
					} else {
						continue;
					}
				case 2:
					if(y < map.getHeight()-1) {
						y++;
						break;
					} else {
						continue;
					}
				case 1:
					// ワールドマップとプレイヤーの現在地
					System.out.println("■ が現在地です。");
					for(int i=0;i<map.getHeight();i++) { // 左右の座標移動
						for(int j=0;j<map.getWidth();j++) { // 上下の座標移動
							if(j % map.getWidth() == 0) System.out.println();
							if(watchingMap[h.getY()] == watchingMap[i] && watchingMap[h.getX()] == watchingMap[j]){
								watchingMap[h.getY()][h.getX()] = "■ ";
								System.out.print(watchingMap[h.getY()][h.getX()]);
							} else {
								watchingMap[i][j] = "□ ";
								System.out.print(watchingMap[i][j]);
							}
						}
					}
					System.out.println();
					continue;
				case 0:
					System.out.print("ゲームを終了しますか？ Yes:0 No:9 > ");
					int gameEnd = scanner.nextInt();
					if(gameEnd == 0) {
						endFlag = 1;
						System.out.println("-----------------------------------------");
						System.out.println("お疲れ様でした。悪魔に身体を乗っ取られぬよう、お気をつけて・・・。");
						break;
					} else if (gameEnd == 9) {
						System.out.println("ゲームを続けます。");
						System.out.println("-----------------------------------------");
						System.out.println("どこに向かいますか？");
						System.out.print("北:8 東:6 南:2 西:4 マップを見る:1 ゲームの終了:0 > ");
					} else {
						if(gameEnd == 0) break;
						continue;
					}
				default:
					continue;
				}
				break;
			}
			// エンドフラグが1の場合にゲームを終了する。
			if(endFlag == 1) break;
			// 座標更新
			h.setXY(x, y);
			// イベントの取得
			Event e = map.getEvent(x, y);
			// イベントがない場合
			if(e == null) {
				nullEvent();
				continue;
			}

			// イベントがある場合
			Event.EventType t = e.getType();

			if(t == Event.EventType.Talk) {
				System.out.println();
				System.out.println("イベント発生！ "+"「"+e.getName()+"」");
				System.out.println("何者かが、「"+h.getName()+"」に話しかけてきた。");
				talk(e.getName());

			} else if(t == Event.EventType.Battle) {
				System.out.println();
				System.out.println("イベント発生！ "+"「"+e.getName()+"」");
				System.out.println("モンスターが現れた！");
				Monster m = e.getMonster();
				battle(h,m);

			} else if (t == Event.EventType.BossBattle) {
				System.out.println();
				System.out.println("イベント発生！ "+"「"+e.getName()+"」");
				Monster m = e.getMonster();
				clearFlag = bossBattle(h,m);
				if(clearFlag == 1) {
					gameEnd();
					break;
				}
			} else if (t == Event.EventType.AliceBattle) {
				System.out.println();
				System.out.println("イベント発生！ "+"「"+e.getName()+"」");
				Monster m = e.getMonster();
				clearFlag = aliceBattle(h,m);
				if(clearFlag == 1) {
					gameEnd();
					break;
				}
			} else if (t == Event.EventType.Care){
				System.out.println();
				System.out.println("イベント発生！ "+"「"+e.getName()+"」");
				h.setLife(h.getMaxLife());
				h.setMagic(h.getMaxMagic());
				System.out.println("「"+h.getName()+"」が全回復 ["+ h.getLife()+"] ["+ h.getMagic()+"]");

			} else if (t == Event.EventType.Damage) {
				System.out.println();
				System.out.println("イベント発生！ "+"「"+e.getName()+"」");
				h.setLife(h.getLife() - e.getDamagePoint());
				System.out.println(h.getName()+"が"+e.getDamagePoint()+"ダメージを受けた！");
				if(h.getLife() <= 0) {
					System.out.println(h.getName()+"が死んでしまった・・・。");

					gameOverFlag = gameOver();
					if(gameOverFlag == 1) {
						// プレイヤーのHP,MPを回復してスタート地点へ。
						gameOverFlag = 0;
						h.setLife(h.getMaxLife());
						h.setMagic(h.getMaxMagic());
						h.setXY(0, 0);
						continue;
					} else if(gameOverFlag == 0) {
						break;
					}
				}
			}
		}
		scanner.close();
	}

	private static WorldMap makeMap(Scanner scanner) {

		// mapの大きさ
		WorldMap map = new WorldMap(5, 5);

		// モンスターの種類
		// Name、最大HP、HP、最大MP、MP、攻撃力、素早さ
		Monster m11 = new Monster("バイオレンスおやじ", 100, 100, 0, 0, 10, 20);
		Monster m12 = new Monster("炸裂する小悪魔", 30, 30, 0, 0, 15, 80);
		Monster m13 = new Monster("渇きの老婆", 200, 200, 30, 30, 30, 0);
		Monster m14 = new Monster("ケルベロス", 120, 120, 0, 0, 20, 70);
		Monster m15 = new Monster("デーモンコア", 100, 100, 0, 0, 40, 0);

		Monster m16 = new Monster("Lucifer", 666, 666, 444, 444, 20, 100);
		Monster m17 = new Monster("Alice", 1000, 1000, 666, 666, 15, 100);


		// イベントの生成と登録
		Event e01 = new Event(Event.EventType.Talk, "見習いの旅人");
		Event e02 = new Event(Event.EventType.Talk, "傷ついた戦士");
		Event e03 = new Event(Event.EventType.Talk, "かわいそうな女の子");
		Event e04 = new Event(Event.EventType.Talk, "憎たらしい悪魔");
		Event e05 = new Event(Event.EventType.Talk, "みすぼらしい浮浪者");
		Event e06 = new Event(Event.EventType.Talk, "疲れ果てた長老");
		Event e07 = new Event(Event.EventType.Talk, "闇夜の誘惑を支配する者");
		Event e08 = new Event(Event.EventType.Talk, "死を司る殺戮の天使");

		Event e11 = new Event(Event.EventType.Battle, "悪魔に侵されたおやじ");
		e11.setMonster(m11);
		Event e12 = new Event(Event.EventType.Battle, "狂気の時限爆弾");
		e12.setMonster(m12);
		Event e13 = new Event(Event.EventType.Battle, "泉を貪るもの");
		e13.setMonster(m13);
		Event e14 = new Event(Event.EventType.Battle, "地獄の番犬");
		e14.setMonster(m14);
		Event e15 = new Event(Event.EventType.Battle, "浮遊する悪魔の心臓");
		e15.setMonster(m15);

		Event e16 = new Event(Event.EventType.BossBattle, "輝く者が天より墜ちた");
		e16.setMonster(m16);
		Event e17 = new Event(Event.EventType.AliceBattle, "謎の少女");
		e17.setMonster(m17);

		Event e21 = new Event(Event.EventType.Care, "回復の泉");
		e21.setCareLife(0);

		Event e22 = new Event(Event.EventType.Damage, "落とし穴");
		e22.setDamagePoint(100);
		Event e23 = new Event(Event.EventType.Damage, "強烈な電流");
		e23.setDamagePoint(200);

		// マップ上の座標を指定して登録
		map.setEvent(e01, 0, 1);
		map.setEvent(e02, 1, 0);
		map.setEvent(e03, 1, 2);
		map.setEvent(e04, 3, 3);
		map.setEvent(e05, 3, 2);
		map.setEvent(e06, 2, 1);
		map.setEvent(e07, 0, 4);
		map.setEvent(e08, 4, 0);

		map.setEvent(e11, 1, 1);
		map.setEvent(e12, 0, 2);
		map.setEvent(e13, 0, 3);
		map.setEvent(e14, 2, 3);
		map.setEvent(e14, 4, 2);
		map.setEvent(e15, 4, 1);

		map.setEvent(e16, 4, 4);
		map.setEvent(e17, 2, 2);

		map.setEvent(e21, 0, 2);
		map.setEvent(e21, 2, 4);

		map.setEvent(e22, 3, 0);
		map.setEvent(e23, 1, 3);
		map.setEvent(e23, 4, 3);

		return map;
	}

	private static Hero makeHero(Scanner scanner) {
		Random r = new Random();
		System.out.println("主人公の名前を入力してください。");
		System.out.print("主人公の名前:");
		while(true) {
			String name = scanner.nextLine();
			// Name、最大HP、HP、最大MP、MP、攻撃力、素早さ
			Hero hero = new Hero(name, 200, 200, 100, 100,200+r.nextInt(10), 21+r.nextInt(20));
			if(hero.getName().equals("")) {
				System.out.println("それはnullだ。");
				System.out.print("主人公の名前:");
				continue;
			}
			return hero;
		}
	}

	private static void battle(Character a, Character b) {

		Random random = new Random();

		System.out.println("-----------------------------------------");
		System.out.println(b.getName() + "が現れた！");

		while (true) {
			System.out.println();
			System.out.println(a.getName() + " :Life " + a.getLife() + " :Magic " + a.getMagic());
			System.out.println(b.getName() + " :Life " + b.getLife());
			System.out.println("-----------------------------------------");

			// 行動順はランダムに決める。運が悪かったら永遠に攻撃される。
			switch (random.nextInt(5)) {
			case 0:
				a.attack(b);
				break;
			case 1:
				a.attack(b);
				break;
			case 2:
				// モンスターがヒーローよりも素早さが高い場合、モンスターのターンになる。
				if (b.getSpeedPoint() > a.getSpeedPoint()) {
					b.attack(a);
					break;
				}
				a.attack(b);
				break;
			case 3:
				// case 2の逆パターン。
				if (a.getSpeedPoint() > b.getSpeedPoint()) {
					a.attack(b);
					break;
				}
				b.attack(a);
				break;
			case 4:
				b.attack(a);
				break;
			}
			// モンスターから逃げるフラグ
			if (b.getEscapeFlag() == 1) {
				break;
			}
			// 全滅
			if (a.getLife() <= 0) {
				System.out.println();
				System.out.println(a.getName() + "のLifeは0になった！");
				System.out.println(a.getName() + "は力尽きた・・・。");
				int gameOverFlag = gameOver();
				if(gameOverFlag == 1) {
					gameOverFlag = 0;
					a.setLife(a.getMaxLife());
					System.out.println();
					continue;
				}
				break;
			}
			// 勝利
			if (b.getLife() <= 0) {
				b.setLife(b.getMaxLife()); // 再戦闘時にモンスターのHPを0のままにしない
				a.setKillMonster(a.getKillMonster()+1); // モンスターを倒した回数を1ずつカウント
				System.out.println();
				System.out.println(b.getName() + "のLifeは0になった！");
				System.out.println(b.getName() + "を倒した！");
				break;
			}
		}

		System.out.println("戦闘終了！");
		System.out.println("-----------------------------------------");
	}

	private static int bossBattle(Character c, Character d) {

		Random random = new Random();
		int clearFlag = 0;

		System.out.println("-----------------------------------------");

		System.out.println("？？？ : フッフッフ・・・。よくぞ、ここまで辿り着いたな。");
		System.out.println("？？？ : 我が名は" + d.getName() + "。");
		System.out.println(d.getName() + " : 悪魔の王にして、この世界の頂点に君臨する者なり。");
		System.out.println(d.getName() + " :人の子よ。お前の持つ力を見せてもらおうか・・・！");

		while (true) {
			System.out.println("-----------------------------------------");
			System.out.println();
			System.out.println(c.getName() + " :Life " + c.getLife() + " :Magic " + c.getMagic());
			if((d.getMaxLife()/4) > d.getLife()) {
				System.out.println(d.getName() + " :Life " + d.getLife());
			} else {
				System.out.println(d.getName() + " :Life ？？？");
			}
			System.out.println("-----------------------------------------");

			// 行動順はランダムに決める。運が悪かったら永遠に攻撃される。
			switch (random.nextInt(5)) {
			case 0:
				c.attack(d);
				break;
			case 1:
				c.attack(d);
				break;
			case 2:
				// モンスターがヒーローよりも素早さが高い場合、モンスターのターンになる。
				if (c.getSpeedPoint() > d.getSpeedPoint()) {
					d.attack(c);
					break;
				}
				c.attack(d);
				break;
			case 3:
				// case 2の逆パターン。
				if (c.getSpeedPoint() > d.getSpeedPoint()) {
					c.attack(d);
					break;
				}
				d.attack(c);
				break;
			case 4:
				d.attack(c);
				break;
			}
			// モンスターから逃げるフラグ
			if (d.getEscapeFlag() == 1) {
				break;
			}
			// 全滅
			if (c.getLife() <= 0) {
				System.out.println();
				System.out.println(c.getName() + "のLifeは0になった！");
				System.out.println(c.getName() + "は力尽きた・・・。");
				int gameOverFlag = gameOver();
				if(gameOverFlag == 1) {
					// 敵のHPとプレイヤーのHPMPを回復して再戦。
					gameOverFlag = 0;
					d.setLife(d.getLife()+((d.getMaxLife()-d.getLife())/2));
					c.setLife(c.getMaxLife());
					c.setMagic(c.getMaxMagic());
					System.out.println();
					System.out.println(d.getName() + " : ほう・・・？まだ立ち上がるか。");
					System.out.println(d.getName() + " : ならば、最後の希望の残滓まで粉々に打ち砕くのみ！");
					System.out.println(d.getName() + " : 我が圧倒的な力の前に、何度でも絶望するがよい！！");
					continue;
				}
				break;
			}
			// 勝利
			if (d.getLife() <= 0) {
				clearFlag = 1;
				System.out.println();
				System.out.println(d.getName() + "のLifeは0になった！");
				System.out.println(d.getName() + "を倒した！");
				break;
			}
		}

		System.out.println("-----------------------------------------");
		if(clearFlag == 1) {
			System.out.println(d.getName() + "の心臓にとどめの一撃を与えた！");
			System.out.println(d.getName() + "は膝から崩れ落ち、翼の先から炎が燃え広がりだした。");
			System.out.println();
			System.out.println(d.getName() + " : なん・・・だと・・・！ 我が力が・・・人間ごときに・・・！");
			System.out.println();
			System.out.println(d.getName() + "は苦悶の表情を浮かべながら、この世のものとは思えない形相でこちらを睨みつける。");
			System.out.println();
			System.out.println(d.getName() + " : おのれ！覚えておけ人間よ！");
			System.out.println(d.getName() + " : 我は何度でも蘇り！その度に絶望を与える！！");
			System.out.println(d.getName() + " : これで終わったと思うな！！");
			System.out.println(d.getName() + " : やがて訪れる我が再臨に恐れおののくがよい！！");
			System.out.println();
			System.out.println(d.getName() + "は高笑いをしながら灰になっていった・・・。");
			System.out.println();
			System.out.println(c.getName() + " : これで・・・終わった・・・のか・・・？");
		}
		return clearFlag;

	}

	private static int aliceBattle(Character e, Character f) {

		Random random = new Random();
		int clearFlag = 0;
		int BattleFlag = aliceTalk(e.getKillMonster());

		if(BattleFlag == 1) {
			while (true) {
				System.out.println("-----------------------------------------");
				System.out.println();
				System.out.println(e.getName() + " :Life " + e.getLife() + " :Magic " + e.getMagic());
				if((f.getMaxLife()/4) > f.getLife()) {
					System.out.println(f.getName() + " :Life " + f.getLife());
				} else {
					System.out.println(f.getName() + " :Life ？？？");
				}
				System.out.println("-----------------------------------------");

				// 行動順はランダムに決める。運が悪かったら永遠に攻撃される。
				switch (random.nextInt(5)) {
				case 0:
					e.attack(f);
					break;
				case 1:
					e.attack(f);
					break;
				case 2:
					// モンスターがヒーローよりも素早さが高い場合、モンスターのターンになる。
					if (e.getSpeedPoint() > f.getSpeedPoint()) {
						f.attack(e);
						break;
					}
					e.attack(f);
					break;
				case 3:
					// case 2の逆パターン。
					if (e.getSpeedPoint() > f.getSpeedPoint()) {
						e.attack(f);
						break;
					}
					f.attack(e);
					break;
				case 4:
					f.attack(e);
					break;
				}
				// モンスターから逃げるフラグ
				if (f.getEscapeFlag() == 1) {
					break;
				}
				// 全滅
				if (e.getLife() <= 0) {
					System.out.println();
					System.out.println(e.getName() + "のLifeは0になった！");
					System.out.println(e.getName() + "は力尽きた・・・。");
					int gameOverFlag = aliceBattleGameOver();
					if(gameOverFlag == 1) {
						// 敵のHPとプレイヤーのHPMPを回復して再戦。
						gameOverFlag = 0;
						f.setLife(f.getLife()+((f.getMaxLife()-f.getLife())/2));
						e.setLife(e.getMaxLife());
						e.setMagic(e.getMaxMagic());
						System.out.println();
						System.out.println(f.getName() + " : はい！ はやくたって！");
						System.out.println(f.getName() + " : もうげんきになったでしょ？");
						System.out.println(f.getName() + " : それじゃあ、また"+e.getName()+"をころすね！");
						continue;
					}
					break;
				}
				// 勝利
				if (f.getLife() <= 0) {
					clearFlag = 1;
					System.out.println();
					System.out.println(f.getName() + "のLifeは0になった！");
					System.out.println(f.getName() + "を倒した！");
					break;
				}
			}

			System.out.println("-----------------------------------------");
			if(clearFlag == 1) {

				System.out.println(f.getName() + " : うぬぬ・・・！");
				System.out.println(f.getName() + " : こうさーん！！！");
				System.out.println();
				System.out.println(f.getName() + "は服をパタパタさせながら敗北宣言をした。");
				System.out.println("どうやら、あの行動で白旗を振っていることを示しているらしい。");
				System.out.println();
				System.out.println(f.getName() + " : あーあ。まけちゃった・・・。");
				System.out.println(f.getName() + " : でも、たのしかったね！");
				System.out.println(f.getName() + " : あそびもおわったし、あとかたづけをしなきゃ！");
				System.out.println();
				System.out.println(f.getName() + "はおもむろに Luciferのいる方角を向いた。");
				System.out.println(f.getName() + "は両手を前にかざすと、"+e.getName()+"が感じたことのない魔力を束ねはじめた。");
				System.out.println();
				System.out.println(f.getName() + " : ハデスがもってるバイデントという「やり」をつくったの。");
				System.out.println(f.getName() + " : これでLuciferをたおすわ！ えーい！");
				System.out.println();
				System.out.println("バイデントがLuciferの身体に突き刺さった。");
				System.out.println("Lucifer : グウオオォォォォォオオォォォ！！！！ なんだこれはああぁぁああぁあ！！！！");
				System.out.println(f.getName() + "は槍で貫かれた苦しみを受けながら灰になっていった・・・。");
				System.out.println();
				System.out.println(e.getName() + " : これで・・・終わった・・・のか・・・？");
				System.out.println();
				System.out.println(f.getName() + " : おわったのよ。えっへん！");
			}
		}
		return clearFlag;

	}


	private static void gameStart() {
		System.out.println("-----------------------------------------");
		System.out.println("この世界は、悪魔によって支配されてしまいました。");
		System.out.println("あなたは、世界を取り戻す救世主です。");
		System.out.println("悪魔の王「Lucifer」を倒し、世を平穏に導いてください。");
		System.out.println();
		System.out.println();
		System.out.println("「the Authentic Descent of the Goddess」");
		System.out.println();
		System.out.println();
		System.out.println("-----------------------------------------");
	}

	private static int gameOver() {
		Scanner sc = new Scanner(System.in);
		Random random = new Random();
		int gameRetryFlag = 0;

		System.out.println("-----------------------------------------");
		System.out.println("意識がだんだん遠のいていく・・・。");
		System.out.println("気がつくと、とても美しい風景の河原にいた。");
		System.out.println("川の向こうから、賑やかな祭囃子が聞こえてくる。");
		System.out.println("・・・頭の中に声が響いた。");
		System.out.println();
		System.out.println("？？？ : 志半ばで地に伏した人の子よ。そなたにはまだ為すべきことがある。");
		System.out.println("？？？ : 混沌の世界を束ねし魔王を倒すという使命がある。");
		System.out.println("？？？ : ・・・まだこの川を渡るべきではない。");
		System.out.println("？？？ : もう一度、転生するのだ！行け！");
		System.out.println();

		int gameOvertalk = random.nextInt(3);
		switch (gameOvertalk) {
		case 0:
			System.out.println("Sariel : 限界を迎えてしまったか。人の子よ。");
			System.out.println("Sariel : ここで諦め、安息を求めるもよし。また立ち上がり、戦いを選ぶも良い。");
			System.out.println("Sariel : 私としては、ルシファーを倒して欲しいのだがな・・・。");
			break;
		case 1:
			System.out.println("Lilith : ・・・ちょっと！しっかりしてよ！");
			System.out.println("Lilith : あんなのに負けちゃってどうするのよ！");
			System.out.println("Lilith : ここで諦めるなんて絶対にダメなんだからね！");
			break;
		case 2:
			System.out.println("Alice : あれー？もうしんじゃったのー？");
			System.out.println("Alice : もうちょっとがんばってくれるとおもったんだけどなー。");
			System.out.println("Alice : ねえねえ、あたしといっしょにくらしてくれるのは、まだみらいのはなしだよね？");
			break;
		}
		System.out.println();
		while(true) {
			System.out.print("選択肢: 立ち上がる:0 諦める:9 > ");
			int gameEnd = sc.nextInt();
			System.out.println();
			System.out.println();
			if(gameEnd == 0) {
				gameRetryFlag = 1;
				System.out.println();
				break;
			} else if(gameEnd == 9) {
				System.out.println("「Game Over」");
				System.out.println();
				System.out.println();
				System.out.println("-----------------------------------------");
				break;
			} else {
				continue;
			}
		}

		if(gameRetryFlag == 1) {
			switch (gameOvertalk) {
			case 0:
				System.out.println("Sariel : そうか、戦いを選ぶか。");
				System.out.println("Sariel : お前は強い人間だ。 必ずLuciferを倒すことができる。");
				System.out.println("Sariel : 神のご加護がありますよう・・・。");
				break;
			case 1:
				System.out.println("Lilith : そうこなくっちゃね！");
				System.out.println("Lilith : さっさとLuciferを倒して、昔に戻るわよ！");
				System.out.println("Lilith : えっ・・・？ 戻る方法を知らない？ ・・・な、なんとかなるわ！");
				break;
			case 2:
				System.out.println("Alice : ・・・だよね！ まだやることがのこってるもんね！");
				System.out.println("Alice : あたしはさみしくなんかないから・・・。だいじょうぶ！");
				System.out.println("Alice : わるいやつはみんなたおしちゃえ！ がんばれー！");
				break;
			}
			System.out.println("急に意識が引き戻される感覚に陥った。");
			System.out.println("・・・また、頭の中に声が響いた。");
			System.out.println();
			System.out.println("？？？ : ・・・再度、転生を行った。");
			System.out.println("？？？ : では、行くがよい。");
		}
		sc.close();
		return gameRetryFlag;
	}
	private static int aliceBattleGameOver() {
		Scanner sc = new Scanner(System.in);
		int gameRetryFlag = 0;

		System.out.println("-----------------------------------------");
		System.out.println("意識がだんだん遠のいていく・・・。");
		System.out.println("気がつくと、とても美しい風景の河原にいた。");
		System.out.println("川の向こうから、賑やかな祭囃子が聞こえてくる。");
		System.out.println("・・・頭の中に声が響いた。");
		System.out.println();
		System.out.println("？？？ : 志半ばで地に伏した人の子よ。そなたにはまだ為すべきことがある。");
		System.out.println("？？？ : 混沌の世界を束ねし魔王を倒すという使命がある。");
		System.out.println("？？？ : ・・・まだこの川を渡るべきではない。");
		System.out.println("？？？ : もう一度、転生するのだ！行け！");
		System.out.println();
		System.out.println("耳元でAliceが囁く。");
		System.out.println();
		System.out.println("Alice : どう？ あたしつよいでしょ？");
		System.out.println("Alice : このかわをわたったらね、あたしたち、かぞくになれるんだよ？");
		System.out.println("Alice : あたしとずーっといっしょにいてくれる？");
		System.out.println();

		while(true) {
			System.out.print("選択肢: まだ戦う:0 ありすといっしょにくらす:9 > ");
			int gameEnd = sc.nextInt();
			System.out.println();
			System.out.println();
			if(gameEnd == 0) {
				gameRetryFlag = 1;
				System.out.println();
				System.out.println("Alice : ・・・けち。");
				System.out.println("Alice : いいもん！わかってくれるまで、ここにつれてくるんだから！");
				System.out.println();
				System.out.println("急に意識が引き戻される感覚に陥った。");
				System.out.println("・・・また、頭の中に声が響いた。");
				System.out.println();
				System.out.println("？？？ : ・・・再度、転生を行った。");
				System.out.println("？？？ : では、行くがよい。");
				break;
			} else if(gameEnd == 9) {
				System.out.println();
				System.out.println("Alice : やったぁ！ あたしうれしい！");
				System.out.println("Alice : これからはずっといっしょだね！");
				System.out.println("Alice : ずーっと、ずーっと、えいえんに・・・。");
				System.out.println();
				System.out.println("-----------------------------------------");
				System.out.println();
				System.out.println();
				System.out.println("「Game Over」");
				System.out.println();
				System.out.println();
				break;
			} else {
				continue;
			}
		}
		sc.close();
		return gameRetryFlag;
	}

	private static void nullEvent() {
		Random random = new Random();
		switch (random.nextInt(3)) {
		case 0:
			System.out.println("遠くから悪魔の笑い声が聞こえる・・・。");
			break;
		case 1:
			System.out.println("痩せた大地に萎れかけた花が咲いている・・・。");
			break;
		case 2:
			System.out.println("人の断末魔が聞こえた気がした・・・。");
			break;
		}
	}

	private static void talk(String talkName) {
		System.out.println("-----------------------------------------");
		if(talkName.equals("見習いの旅人")) {
			System.out.println("見習いの旅人 : 「見慣れない顔だな。悪魔の退治に来たのか？」");
			System.out.println("見習いの旅人 : 「お前のような勇敢な者は全てルシファーに返り討ちにされた。」");
			System.out.println("見習いの旅人 : 「お前もそうならないように祈っているよ。」");

		} else if(talkName.equals("傷ついた戦士")) {
			System.out.println("傷ついた戦士 : 「よう・・・。 悪魔討伐は順調か？」");
			System.out.println("傷ついた戦士 : 「戦いで傷ついた時は回復の泉に行くといいぜ。」");
			System.out.println("傷ついた戦士 : 「いくつか回復の泉はあるらしい。俺も今から行くか・・・。」");

		} else if(talkName.equals("かわいそうな女の子")) {
			System.out.println("かわいそうな女の子 : 「パパとママが悪魔にやられちゃった・・・。」");
			System.out.println("かわいそうな女の子 : 「あたしを守るために悪魔と一緒に崖から落ちたの。」");
			System.out.println("かわいそうな女の子 : 「あたし、一人ぼっちでどうやって生きていくの・・・？」");

		} else if(talkName.equals("憎たらしい悪魔")) {
			System.out.println("憎たらしい悪魔 : 「ヒヒッ、オロカナニンゲンヨ。」");
			System.out.println("憎たらしい悪魔 : 「オレタチハ、トクシュコウゲキヲ、モッテイル。」");
			System.out.println("憎たらしい悪魔 : 「ユダンシテイルト、アットイウマニ、シヌゼ。ヒヒッ。」");

		} else if(talkName.equals("みすぼらしい浮浪者")) {
			System.out.println("みすぼらしい浮浪者 : 「風の噂で聞いたのじゃがな。」");
			System.out.println("みすぼらしい浮浪者 : 「北東と南西にそれぞれ強い悪魔がいるそうじゃ。」");
			System.out.println("みすぼらしい浮浪者 : 「しかし、人間を襲わないらしいぞい。謎じゃのう・・・。」");

		} else if(talkName.equals("疲れ果てた長老")) {
			System.out.println("疲れ果てた長老 : 「この世界はもうダメじゃ・・・。」");
			System.out.println("疲れ果てた長老 : 「南東の方角にいる悪魔の王。」");
			System.out.println("疲れ果てた長老 : 「こいつを倒さん限りは平和は訪れぬ・・・。」");

		} else if(talkName.equals("闇夜の誘惑を支配する者")) {
			System.out.println("一人の妖艶な姿の女が、荒野に佇んでいる。");
			System.out.println();
			System.out.println("？？？ : 久しぶりね・・・。ってこの姿で会うのは初めてかしら？");
			System.out.println("？？？ : あたしは Lilithよ。恥ずかしいから、あんまりジロジロ見ないでね。");
			System.out.println();
			System.out.println("Lilith : 本当はあなたと一緒に戦う予定だったんだけど・・・。");
			System.out.println("Lilith : 何かの間違いで、悪魔として転生しちゃったわ。");
			System.out.println("Lilith : この世界の神には本当に呆れた・・・。なんで人間と悪魔を間違えるのよ。");
			System.out.println("Lilith : そうそう、あなたが倒すべき「Lucifer」は[4,4]にいるわ。");
			System.out.println("Lilith : 遠くから、あなたの無事と勝利を祈っているね。");

		} else if(talkName.equals("死を司る殺戮の天使")) {
			System.out.println("強固な封印の中で、1体の堕天使が横たわっている。");
			System.out.println();
			System.out.println("？？？ : 君は救世主か・・・。");
			System.out.println("？？？ : 私の名は Sariel。");
			System.out.println();
			System.out.println("Sariel : 私は、これまでの自分の行いを思い出してしまった。");
			System.out.println("Sariel : 「Lucifer」に操られていたとはいえ、酷いことをした。");
			System.out.println("Sariel : 幼い子供、若い娘、罪のない老人の命を奪ってしまった。");
			System.out.println("Sariel : 今の私は、「Lucifer」によって封印されている。");
			System.out.println("Sariel : 人の子よ。「Lucifer」を倒してくれ。");
			System.out.println("Sariel : そして私に罪を償わせてくれ・・・。");
		}
	}

	private static int aliceTalk(int killCount) {
		Scanner sc = new Scanner(System.in);
		int BattleFlag = 0;
		System.out.println("-----------------------------------------");
		System.out.println("青白い顔の少女が、じっとこちらを見つめてくる。");
		System.out.println();
		System.out.println("？？？ : はじめまして・・・だよねー？");
		System.out.println("？？？ : あたしのなまえは Alice！");
		System.out.println();
		System.out.println("Alice : じつはね！とおくから きみのことをみていたんだよー。");

		if(killCount == 0) {
			System.out.println("Alice : まだ、あくまをやっつけていないみたいだねー。");
			System.out.println("Alice : こわーいあくまがでてきても、あんまりいじめないでね！");
		} else if(killCount < 3) {
			System.out.println("Alice : "+ killCount +"かい、あくまをやっつけているんだね！");
			System.out.println("Alice : あんまりあくまをいじめちゃダメだよ！");
		} else if(killCount >= 3) {
			System.out.println("Alice : "+ killCount +"かい、あくまをやっつけちゃったんだね・・・。");
			System.out.println("Alice : ねえ、ひとつしつもんしてもいーい？");
			System.out.println();
			System.out.println();
			System.out.println("Alice : あくまをころしてもヘーキなの？");
			System.out.println();
			System.out.println();
			System.out.print("選択肢 : Yes:0 No:9 > ");
			while(true) {
				switch(sc.nextInt()) {
				case 0:
					System.out.println();
					System.out.println("Alice : そうなんだ・・・。");
					System.out.println("Alice : じつは、あたしもあくまなの。");
					System.out.println("Alice : だから、ちょっぴりショックよ・・・。");
					System.out.println("Alice : ・・・でもそんなにころしてるなら、きみは、けっこうつよそうだね！");
					System.out.println("Alice : だったら、あたしのあそびあいてになってよ！");
					System.out.println("Alice : みんなすぐしんじゃうから、あたし、ひまだったんだ！");
					System.out.print("選択肢 : Yes:0 No:9 > ");
					while(true) {
						switch(sc.nextInt()) {
						case 0:
							System.out.println();
							System.out.println("Alice : やった！うふふー！");
							System.out.println("Alice : それじゃあ、ころしあいごっこはじめるね！");
							BattleFlag = 1;
							break;
						case 9:
							System.out.println();
							System.out.println("Alice : ・・・けち。");
							BattleFlag = 2;
							break;
						default :
							continue;
						}
						break;
					}
				case 9:
					if(BattleFlag == 0) {
						System.out.println();
						System.out.println("Alice : そうだよね！ よかったぁ・・・。");
						System.out.println("Alice : おねがいだから、あくまには、やさしくしてね・・・！");
					}
					break;
				default :
					continue;
				}
				break;
			}
		}
		sc.close();
		return BattleFlag;
	}

	private static void gameEnd() {
		System.out.println("-----------------------------------------");
		System.out.println("この世界は、悪魔によって支配されてしまいました。");
		System.out.println("しかし、一人の救世主の手によって、世界に平和が取り戻されました。");
		System.out.println("人々は絶望を乗り越え、また新たな歴史を積み上げていくのであった・・・。");
		System.out.println();
		System.out.println();
		System.out.println("「the Authentic Descent of the Goddess」");
		System.out.println();
		System.out.println();
		System.out.println("                 「The End」                ");
		System.out.println();
		System.out.println();
		System.out.println("-----------------------------------------");
	}
}
