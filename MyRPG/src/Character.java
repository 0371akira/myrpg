public abstract class Character {

	private String name;
	private int maxLife;
	private int life;
	private int maxMagic;
	private int magic;
	private int hitPoint;
	private int speedPoint;
	private int damage;

	private int specialAttackFlag = 0;
	private int escapeFlag = 0;
	private int killMonster = 0;

	private int x;
	private int y;

	// コントラクタ
	// Name、最大HP、HP、最大MP、MP、攻撃力、素早さ、特殊行動フラグ,
	public Character() {
		this("名無し",100,100,0,0,20,0);
	}

	public Character(String _name) {
		this(_name,100,100,0,0,20,0);
	}

	public Character(String _name, int maxLife) {
		this(_name,maxLife,100,0,0,20,0);
	}

	public Character(String _name, int maxLife, int life) {
		this(_name,maxLife,life,0,0,20,0);
	}

	public Character(String _name, int maxLife, int life, int maxMagic) {
		this(_name,maxLife,life,maxMagic,0,20,0);
	}

	public Character(String _name, int maxLife, int life, int maxMagic, int magic) {
		this(_name,maxLife,life,maxMagic,magic,20,0);
	}

	public Character(String _name, int maxLife, int life, int maxMagic, int magic, int hitPoint) {
		this(_name,maxLife,life,maxMagic,magic,hitPoint,0);
	}


	public Character(String _name, int maxLife, int life, int maxMagic, int magic, int hitPoint, int speedPoint) {
		this(_name,maxLife,life,maxMagic,magic,hitPoint,speedPoint,0);
	}

	public Character(String _name, int maxLife, int life, int maxMagic, int magic, int hitPoint, int speedPoint, int specialAttackFlag) {
		this(_name,maxLife,life,maxMagic,magic,hitPoint,speedPoint,specialAttackFlag,0);
	}

	public Character(String _name, int maxLife, int life, int maxMagic, int magic,
			int hitPoint, int speedPoint, int specialAttackFlag, int damage) {
		this.name = _name;
		this.maxLife = maxLife;
		this.life = life;
		this.maxMagic = maxMagic;
		this.magic = magic;
		this.hitPoint = hitPoint;
		this.speedPoint = speedPoint;
		this.specialAttackFlag = specialAttackFlag;
		this.damage = damage;
	}

	public void sayHello() {
		System.out.println("こんにちは" + name + "です。");
	}

	public abstract int attack(Character c);

	// mapのアクセサー
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public void setXY(int x,int y) {
		this.y = y;
		this.x = x;
	}

	// nameのアクセサー
	public String getName() {
		return name;
	}

	public void setName(String _name) {
		this.name = _name;
	}

	// maxLifeのアクセサー
	public int getMaxLife() {
		return maxLife;
	}

	public void setMaxLife(int maxLife) {
		this.maxLife = maxLife;
	}

	// lifeのアクセサー
	public int getLife() {
		return life;
	}

	public void setLife(int life) {
		this.life = life;
	}

	// maxMagicのアクセサー
	public int getMaxMagic() {
		return maxMagic;
	}

	public void setMaxMagic(int maxMagic) {
		this.maxMagic = maxMagic;
	}

	// magicのアクセサー
	public int getMagic() {
		return magic;
	}

	public void setMagic(int magic) {
		this.magic = magic;
	}

	// hitPointのアクセサー
	public int getHitPoint() {
		return hitPoint;
	}

	public void setHitPoint(int hitPoint) {
		this.hitPoint = hitPoint;
	}

	// speedPointのアクセサー
	public int getSpeedPoint() {
		return speedPoint;
	}

	public void setSpeedPoint(int speedPoint) {
		this.speedPoint = speedPoint;
	}

	// escapeFlagのアクセサー
	public int getEscapeFlag() {
		return escapeFlag;
	}

	public void setEscapeFlag(int escapeFlag) {
		this.escapeFlag = escapeFlag;
	}

	// specialAttackFlagのアクセサー
	public int getSpecialAttackFlag() {
		return specialAttackFlag;
	}

	public void setSpecialAttackFlag(int specialAttackFlag) {
		this.specialAttackFlag = specialAttackFlag;
	}

	// damegeのアクセサー
	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	// killMonsterのアクセサー
	public int getKillMonster() {
		return killMonster;
	}

	public void setKillMonster(int killMonster) {
		this.killMonster = killMonster;
	}
}